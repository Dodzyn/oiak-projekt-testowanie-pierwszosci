#include "dzialania.h"

/*
 * Funkcja bignum_sub_s
 * -------------
 *  odejmuje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *  jeden z nich jest przedtem skalowany
 *  wynik jest liczony według wzoru
 *  r = a - b*256^s
 *
 *  a: liczba całkowita ustalonego rozmiaru 
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: bufor wyniku
 *  s: skala
 *
 *  returns: overflow (pożyczka)
 */
int bignum_sub_s(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r, int s)
{
    unsigned char ov = 0;
    for(int i = 0; i < a_size; ++i)
    {
        unsigned char a_byte = a[i];

        int skalowany_indeks = i - s ;
        unsigned char b_byte;
        if (skalowany_indeks < 0 || skalowany_indeks >= b_size)
            b_byte = 0;
        else
            b_byte = b[skalowany_indeks];

        int w = a_byte - b_byte - ov;
        r[i] = w;
        if(w < 0) ov = 1;
        else ov = 0;
    }
    return ov;
}

/*
 * Funkcja: bignum_add_s
 * -------------
 *  dodaje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *  jeden z nich jest przedtem skalowany
 *  wynik jest liczony według wzoru
 *  r = a + b*256^s
 *
 *  a: liczba całkowita ustalonego rozmiaru 
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: bufor wyniku
 *  s: skala
 *
 *  returns: overflow (przeniesienie)
 */

int bignum_add_s(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r, int s)
{
    unsigned char ov = 0;
    for(int i = 0; i < a_size; ++i)
    {
        unsigned char a_byte = a[i];
        int skalowany_indeks = i - s ;
        unsigned char b_byte;
        if (skalowany_indeks < 0 || skalowany_indeks >= b_size)
            b_byte = 0;
        else
            b_byte = b[skalowany_indeks];

        int w = a_byte + b_byte + ov;
        r[i] = w;
        if(w < 256) ov = 0;
        else ov = 1;
    }
    return ov;
}

/*
 * Funkcja: bignum_sub
 * -------------
 *  odejmuje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *
 *  a: liczba całkowita ustalonego rozmiaru
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: a - b bufor wyniku
 *
 *  returns: overflow (pożyczka)
 */

int bignum_sub(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r)
{
    return bignum_sub_s(a, a_size, b, b_size, r, 0);
}

/*
 * Funkcja: bignum_add
 * -------------
 *  dodaje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *
 *  a: liczba całkowita ustalonego rozmiaru
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r:  a + b bufor wyniku
 *
 *  returns: overflow (przeniesienie)
 */

int bignum_add(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r)
{
    return bignum_add_s(a, a_size, b, b_size, r, 0);
}

/*
 * Funkcja: mul_c
 * ---------------
 *  mnożenie argumentu rozmiaru kilku słów unsigned przez stałą
 *
 *  a: argument o stałej długości unsigned
 *  b: stała
 *  r: bufor wyniku
 *
 *  returns: najbardziej znaczące słow wyniku
 */
unsigned char mul_c(const unsigned char a[BIG_NUM_BYTES],const unsigned char b, unsigned char r[BIG_NUM_BYTES])
{
    for(int i = 0; i < BIG_NUM_BYTES; ++i)
    {
        r[i] = 0;
    }
    unsigned char c = 0;
    for(int i = 0; i < BIG_NUM_BYTES; ++i)
    {
        unsigned short res = a[i] * b + c;
        r[i] = res;
        c = res >> 8;
    }
    return c;
}

/*
 * Funkcja: bignum_div
 * -------------
 *  dzielenie dwóch argumentów bez znaku rozmiaru kilku słów i zwraca wynik
 *  używając algorytmu optymalnego dzielenia 14.20 z 
 *  Handbook of Applied Cryptography by A. Menezes, P. van Oorschot and S. Vanstone., rozdział 14
 *
 *  a, b: argumenty o ustalonym rozmiarze unsigned
 *  r: bufor na resztę z dzielenia
 *  q: bufor na iloraz
 *
 *  returns: 1 (TRUE) jeśli dzielenie jest poprawne, 0 (FALSE) jeśli nie(dzielenie przez 0)
 */
int bignum_div(const unsigned char * a, int a_size, const unsigned char * b, 
        int b_size, unsigned char * r, unsigned char * q)
{
    // zapisanie kopii a i b oraz alokacja pamięci dla ilorazu
    unsigned char x[a_size];
    unsigned char q_ret[a_size];
    unsigned char overflow[a_size];
    // wyzerowanie
    for(int i = 0; i < a_size; ++i)
    {
        x[i] = a[i];
        q_ret[i] = 0;
        overflow[i] = 0;
    }

    // obliczenie ilości zer w liczbie a
    int n = 0;
    for(int i = a_size - 1; i >= 0; --i)
    {
        if(a[i] == 0)
        {
            ++n;
        }
        else break;
    }
    // najmniej znacząca pozycja a[] jest równa (a_size - 1) - n
    n = a_size - 1 - n;

    // obliczenie ilości zer w liczbie b
    int t = 0;
    for(int i = b_size - 1; i >= 0; --i)
    {
        if(b[i] == 0)
        {
            ++t;
        }
        else break;
    }

    // najmniej znacząca pozycja b[] jest równa (b_size - 1) - t
    t = b_size - 1 - t;
    if(t < 0)
    {
        printf("Division by 0!");
        return 0;
    }

    // jeśli t jest większe, to q_ret[] jest 0 oraz q_ret[] jest a[]
    if(t > n)
    {
        if (q != NULL)
        {
            for(int i = 0; i < a_size; ++i)
                q_ret[i] = 0;
        }
        if (r != NULL) memcpy(r, a, b_size);
        return 0;
    }


    // przesunięcie o skale
    int scale = n - t;
    // dopóki y*2^(n - t) >= x 
    int ov = bignum_sub_s(x, a_size, b, b_size, x, scale);
    while(!ov)
    {
        ++q_ret[n - t];
        ov = bignum_sub_s(x, a_size, b, b_size, x, scale);
    }
    bignum_add_s(x, a_size, b, b_size, x, scale);
    
    for(int i = n; i > t; --i)
    {
        scale = i - t - 1;
        unsigned short q_res = 0;
        if(x[i] == b[t])
        {
            q_res = 0xff; 
        }
        else
        {
            q_res = ((x[i] << 8) ^ x[i - 1]) / b[t];
        }

        while(q_res * ((b[t] << 8) ^ b[t-1]) > ((x[i] << 2*8) ^ (x[i - 1] << 8) ^ (x[i - 2])) )
        {
            --q_res;
        }
        unsigned char mul_res[b_size];
        overflow[0] = mul_c(b,q_res,mul_res);
        bignum_add(mul_res, b_size, overflow, a_size, mul_res);
        int ov = bignum_sub_s(x, a_size, mul_res, b_size, x, scale);
        if(ov)
        {
            bignum_add_s(x, a_size, b, b_size, x, scale);
            --q_res;
        }

        q_ret[scale] = q_res;

    }
    if (r != NULL)
    {
        for(int i = 0; i < b_size; ++i)
            r[i] = x[i];
    }
    if (q != NULL)
    {
        for(int i = 0; i < a_size; ++i)
            q[i] = q_ret[i];
    }
    
    return 1;
}

/*
 * Funkcja: bignum_mul
 * -------------
 *  mnożenie dwóch argumentów bez znaku rozmiaru kilku słów 
 *  używając algorytmu podobnego do 14.12 z
 *  Handbook of Applied Cryptography by A. Menezes, P. van Oorschot and S. Vanstone., rozdział 14
 *
 *  a, b: argumenty o ustalonym rozmiarze unsigned
 *  r: bufor wyniku
 */
void bignum_mul(unsigned char a[BIG_NUM_BYTES], unsigned char b[BIG_NUM_BYTES], unsigned char r[BIG_NUM_BYTES * 2])
{
    for(int i = 0; i < BIG_NUM_BYTES * 2; ++i)
    {
        r[i] = 0;
    }
    for(int i = 0; i < BIG_NUM_BYTES; ++i)
    {
        unsigned char c = 0;
        for(int j = 0; j < BIG_NUM_BYTES; ++j)
        {
            unsigned short res = (unsigned short)b[i] * a[j];
            res += c + r[i + j];
            r[i + j] = res;
            c = res >> 8;
        }
        r[i + BIG_NUM_BYTES + 1] = c;
    }

}

/*
 * Function: bignum_shl
 * --------------
 *  przesunięcie argumentu unsigned
 *
 *  n: argument o ustalonym rozmiarze unsigned
 *  r: bufor wyniku, może być taki sam jak n
 *  nr_size: rozmiar n oraz r
 *  s: ilość bitów do przesunięcia
 */ 
void bignum_shl(unsigned char * n, int n_size, unsigned char * r, int s)
{
    int bytes = s / 8;
    int bits = s % 8; 
    for(int i = 0; i < bytes; ++i)
        r[i] = 0;

    for(int i = n_size - 1; i >= bytes; --i)
    {
        r[i] = n[i - bytes];
    }

    unsigned char high, low;
    low = 0;
    for(int i = bytes; i < n_size; ++i)
    {
        // starsza część to przesunięte bity
        high = 0xff << (8 - bits);
        high = r[i] & high;
        high = high >> (8 - bits);

        // zamiana bitów z młodszą częścią
        r[i] = r[i] << bits;
        r[i] = r[i] ^ low;

        // następna młodsza część jest aktualną starszą
        low = high;
    }
}

/*
 * Funkcja: bignum_equ
 * ------------
 *  sprawdzenie, czy dwie liczby takiej samej długości są sobie równe
 *
 *  a, b: argumenty o ustalonej długości unsigned
 *  ab_size: rozmiary a[] i b[]
 * 
 *  returns: 1(TRUE) jeśli równe, 0(FALSE) jeśli nie
 */
int bignum_equ(unsigned char * a, unsigned char * b, int ab_size)
{
    for(int i = 0; i < ab_size; ++i)
        if(a[i] != b[i]) return FALSE;
    return TRUE;
}

/* Function: bignum_zero
 * --------------
 *  sprawdzenie, czy liczba jest zerem
 *
 *  a: argument o ustalonej długości unsigned
 *  a_size: rozmiar a[]
 *  
 *  returns: 1(TRUE) jeśli równa, 0(FALSE) jeśli nie
 */

int bignum_zero(unsigned char * a, int a_size)
{
    for(int i = 0; i < a_size; ++i)
        if(a[i] != 0) return FALSE;
    return TRUE;
}

void potegowanie_modulo(unsigned char * x, unsigned char * e, unsigned char * n, unsigned char * y)
{
    /**
    * Obliczenie y <- x ^ e (mod n) używając algorytmu szybkiego potęgowania modularnego
    * z Song Y. Yan, 2006. Teoria Liczb w Informatyce
    * Algorytm 2.1.1
    *
    * @param x Wskaźnik na podstawę, tablica z dużą liczbą
    * @param e Wskaźnik na potęgę, tablica z dużą liczbą
    * @param n Wskaźnik na dzielnik, tablica z dużą liczbą
    * @param y Wskaźnik na bufor wyniku, tablica z dużą liczbą
    */

    // bufor akumulatora
    unsigned char c[BIG_NUM_BYTES] = {1};
    // bufor wyniku
    unsigned char r[BIG_NUM_BYTES * 2];
    for(int i = BIG_NUM_BYTES - 1; i >= 0; --i)
    {
        unsigned char word = e[i];
        for(int i = 7; i >= 0; --i)
        {
            // podniesienie do kwadratu
            // r <- c * c
            bignum_mul(c, c, r);
            // r <- c mod n
            bignum_div(r, BIG_NUM_BYTES * 2, n, BIG_NUM_BYTES, c, NULL);
            if(word & 1 << i)
            {
                // jeśli bit wykładnika jest równy '1', to mnożymy
                // r <- c * x
                bignum_mul(c, x, r);
                // c <- r mod n
                bignum_div(r, BIG_NUM_BYTES * 2, n, BIG_NUM_BYTES, c, NULL);
            }
        }
    }
    // skopiowanie akumulatora do wyniku
    for(int i = 0; i < BIG_NUM_BYTES; ++i)
    {
        y[i] = c[i];
    }
}
