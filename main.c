#include "test_millera_rabina.h"
#include "big_num_read.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[])
{
    unsigned char bignum[BIG_NUM_BYTES];
    read_number("number.txt", bignum);
    int k = 4;

    srand(time(NULL));
    printf("Liczba jest %s\n", is_prime(bignum, BIG_NUM_BYTES, k) ? "prawdopodobnie pierwsza" : "złożona");
    //Prawdopodobieństwo błędu
    float p = 1.0f;
    for(int i = k; i > 0; i--)
    {
        p = p / 4;
    }
    //Zgodnie z wnioskiem 2.2.2
    //Song Y. Yan - Teoria liczb w informatyce
    printf("Prawdopodobieństwo błędnego testu wynosi co najwyżej 4^(-%d) (%f)", k, p);
    return 0;
}
