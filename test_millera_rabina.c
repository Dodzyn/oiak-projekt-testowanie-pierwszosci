#include "test_millera_rabina.h"
#include "dzialania.h"
#include <stdlib.h>

void znajdz_d_j(unsigned char * n, int n_size, unsigned char * d, int * j);

int miller_rabin_test(unsigned char * n, int n_size)
{
    /**
    * Algorytm testowania pseudopierwszości Millera-Rabina
    * sprawdza z silnym prawdopodobieństwem, że liczba jest pierwsza
    *
    * @param n Liczba wielokrotnej precyzji
    * @param n_size Rozmiar liczby
    * 
    * @return zwraca FALSE, gdy liczba jest złożona oraz TRUE, gdy jest     prawdopodobnie pierwsza
    */

    // bufory i stałe
    unsigned char d[n_size];
    unsigned char jeden[n_size];
    unsigned char dwa[n_size];
    unsigned char n_minus_jeden[n_size];
    for(int i = 0; i < n_size; ++i)
    {
        jeden[i] = 0;
        dwa[i] = 0;
        n_minus_jeden[i] = 0;
        d[i] = 0;
    }
    dwa[0] = 2;
    jeden[0] = 1;

    // -1(mod n) == (n-1)(mod n)
    bignum_sub(n, n_size, jeden, n_size, n_minus_jeden);

    // znajdź takie d, j, że n - 1 = d*2^j
    int j;
    znajdz_d_j(n, n_size, d, &j);

    // losowanie świadka pierwszości
    // b <- rand() % n + 2
    unsigned char b[n_size];
    for(int i = 0; i < n_size; ++i)
    {
        b[i] = rand();
    }
    bignum_div(b, n_size, n, n_size, b, NULL);
    bignum_add(b, n_size, dwa, n_size, b);

    // y <- b^d (mod n)
    unsigned char y[n_size];
    potegowanie_modulo(b, d, n, y);

    for(int i = 0; i < j; ++i)
    {
        // jeśli i = 1 i (y = 1 lub y = n - 1) to liczba jest prawdopodobnie pierwsza
        if(bignum_equ(y, n_minus_jeden, n_size))
        {
            return TRUE;
        }
        if(i == 0 && (bignum_equ(y, jeden, n_size)))
        {
            return TRUE;
        }
        // jeśli i > 0 i y = 1, to liczba jest złożona
        // jeśli i > 0 i y = (n - 1), liczba jest prawodpodobnie pierwsza
        else if (i > 0)
        {
            if(bignum_equ(y, jeden, n_size)) 
                return FALSE;
        }
        // y <- y^2 (mod n)
        potegowanie_modulo(y, dwa, n, y);
    }
    return FALSE;

}

int is_prime(unsigned char * n, int n_size, int k)
{
    /**
    * Testowanie pierwszości używając algorytmu Millera-Rabina
    *
    * @param n Wskaźnik na liczbę wielokrotnej precyzji
    * @param n_size Liczba elementów w n
    * @param k Ilość powtórzeń testu
    */

    for(; k > 0; --k)
    {
        // jeśli jeden test jest FALSE, to liczba na pewno jest złożona
        if(!miller_rabin_test(n, n_size)){
            return FALSE;
        }
    }
    // jeśli wsyzstkie testy są TRUE, to liczba jest prawodpodobnie pierwsza
    return TRUE;
}

void znajdz_d_j(unsigned char * n, int n_size, unsigned char * d, int * j)
{
    /**
     * Szuka takich liczb d i j, dla których n - 1 = d * 2 ^ j
     *
     * @param n Wskaźnik na liczbę
     * @param n_size Rozmiar liczby
     * @param d Wskaźnik do liczby d, taki sam format i rozmiar jak n
     * @param j Wskaźnik na j, liczba całkowita
     */
    unsigned char jeden[n_size];
    unsigned char dwa[n_size];
    for(int i = 0; i < n_size; ++i)
    {
        jeden[i] = 0;
        dwa[i] = 0;
    }
    jeden[0] = 1;
    dwa[0] = 2;
    *j = 0;

    bignum_sub(n, n_size, jeden, n_size, d);
    // 2^1 = 2, n - 1 jest parzyste oraz
    // podzielne na początku
    // podzielna = true
    int podzielna = TRUE;

    // wynik i reszta z dzielenia
    unsigned char q[n_size];
    unsigned char r[n_size];
    // znajdź największe j że 2^j | n - 1
    while(podzielna)
    {
        // 2^(j+1)
        // jeśli 2^(j+1) | n - 1
        // to j = j + 1 i sprawdzany jest nastepny j
        bignum_div(d, n_size, dwa, n_size, r, q);

        // sprawdzenie, czy reszta z dzielenia jest 0
        if(bignum_zero(r, n_size))
        {
            // jeśli reszta jest 0, j <- j + 1
            // d <- q (iloraz)
            ++(*j);
            for(int i = 0; i < n_size; ++i)
            {
                d[i] = q[i];
            }
        }
        // jeśli nie 2^(j+1) | n - 1
        // to j zostaje i przerywana jest pętla
        else podzielna = FALSE;
    }
}
