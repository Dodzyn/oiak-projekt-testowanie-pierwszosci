#include "big_num_read.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// wczytaj liczbę z pliku
// filename - nazwa pliku
// buffer - tablica bcd w notacji BIG ENDIAN
// zwraca ilość wczytanych cyfr
int read_bcd(const char* filename, char buffer[MAX_DIGITS])
{
    /**
     * Wczytuje liczbę z pliku i przechowuje ją w formie kodu BCD
     *
     * @param filename Ścieżka do pliku, który ma być odczytany
     * @param buffer Bufor na liczbę BCD
     *
     * @return ilość cyfr BCD, które zostały odczytane
     */
    
    //otwarcie pliku
    FILE* file = fopen(filename, "r");
    // wczytywanie cyf dopóki nie nastąpi koniec pliku lub osiągnięto MAX_DIGITS
    int c;
    int i = 0;
    while((c = fgetc(file)) != '\n'
            && i < MAX_DIGITS)
    {
        c -= '0';
        buffer[i] = (char)c;
        i++;
    }
    // zwraca liczbę cyfr
    return i;
}
// konwersja kodu bcd do kodu binarnego
// bcd_arr - binarnie zakodowana tablica cyfr w notacji BIG ENDIAN
// digit_cnt - liczba cyfr w bcd
// dst - tablica znaków rozmiaru BIG_NUM_BYTES, która reprezentuje
// dużą liczbę w notacji LITTLE ENDIAN
void bcd_to_binary(char* bcd_arr, int digit_cnt, unsigned char* dst, int dst_size)
{

     /**
     * Konwersja kodu BCD do kodu binarnego
     * przechowuje dużą liczbę w kodzie binarnym wewnątrz tablicy unsigned char o ustalonym rozmiarze
     * BIG ENDIAN!
     *
     * @param bcd_array Wskaźnik na tablicę BCD
     * @param digit_cnt Ilość cyfr w tablicy BCD
     * @param dst Wskaźnik na docelową tablicę z przekonwertowaną liczbą
     * @param dst_size Rozmiar przekonwertowanej tablicy
     */
    
    // wyzerowanie tablicy dst
    for(int i = 0; i < dst_size; ++i)
    {
        dst[i] = 0;
    }

    int b = 256;    // dzielnik
    // dzielenie dużej liczby i ilorazów częściowych
    // dopóki nie skończą się cyfry oraz
    // q_(n-1) = r_n oraz q_n = 0

    // indeks reszty z dzielenia
    int r_ind = 0;
    // iloraz po każdym dzieleniu
    char q_arr[digit_cnt];
    // łączna ilość odejmowań po każdym dzieleniu
    int sub_cnt;
    do
    {
        // r <- bcd_arr % b 
        int r = 0;
        // łączna ilość dzieleń w tej iteracji
        sub_cnt = 0;

        // dzielenie bcd_array przez b
        // pamiętając, że BCD jest w LITTLE ENDIAN
        int i = 0;
        while(i < digit_cnt)
        {
            
            int a = 0;
            int q = 0;
            // obliczenie binarnej wartości
            a = 10 * r;
            a += bcd_arr[i];
            if(a < b)
            {
                // jeśli binarna wartość jest mniejsza od b, to nie odejmuj
                q = 0;
                r = a;
            }
            else
            {
                // odejmij jeśli binarna wartość jest większa lub równa b
                ++sub_cnt;
                q = 0;
                while(a >= b)
                {
                    a -= b;
                    ++q;
                }
                r = a;
            }
            q_arr[i] = q;
            ++i;
        }
        // bcd_arr <- q_arr
        memcpy(bcd_arr, q_arr, digit_cnt);
        // reszta do dst
        dst[r_ind] = r;
        ++r_ind;
    } while (sub_cnt > 0 || r_ind == dst_size);
}

void read_number(const char* filename, char bignum[BIG_NUM_BYTES])
{
    /**
    * Wczytanie dużej liczby z pliku do buforu unsigned char
    *
    * @param filename ścieżka do pliku tekstowego z liczbą
    * @param bignum Bufor dla liczby wielokrotnej precyzji
    */

    char bcd[MAX_DIGITS];
    //wczytanie wartości BCD z pliku
    int digit_cnt = read_bcd(filename, bcd);
    //konwersja BCD do postaci binarnej
    bcd_to_binary(bcd, digit_cnt, bignum, BIG_NUM_BYTES);
}
