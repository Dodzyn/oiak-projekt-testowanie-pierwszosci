CFLAGS = -g
MAIN_SOURCES = big_num_read.c main.c dzialania.c test_millera_rabina.c
main.out: $(MAIN_SOURCES) 
	gcc -o $@ -g $(MAIN_SOURCES)

clean:
	rm *.out
