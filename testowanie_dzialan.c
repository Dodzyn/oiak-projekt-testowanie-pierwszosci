#include "dzialania.h"
#include <stdlib.h>
#include <time.h>

void randomize(unsigned char * n, int n_size, int msb)
{
    /**
     * Zwraca losową dużą liczbę w tablicy z określoną liczbą bajtów
     * 
     * @param n Wskaźnik na dużą liczbę
     * @param n_size Rozmiar liczby w bajtach
     * @param msb najbardziej znacząca pozycja
     */
    for(int i = 0; i < n_size; ++i)
    {
        unsigned char r = rand();
        if(i < msb)
            n[i] = r;
        else if(i == msb)
            n[i] = r == 0 ? r + 1 : r;
        else n[i] = 0;
    }
}
