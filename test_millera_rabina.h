#pragma once

#define TRUE 1
#define FALSE 0

/**
 * Algorytm testowania pseudopierwszości Millera-Rabina
 * sprawdza z silnym prawdopodobieństwem, że liczba jest pierwsza
 *
 * @param n Liczba wielokrotnej precyzji
 * @param n_size Rozmiar liczby
 * 
 * @return zwraca FALSE, gdy liczba jest złożona oraz TRUE, gdy jest prawdopodobnie pierwsza
 */
int miller_rabin_test(unsigned char * n, int n_size);

/**
 * Testowanie pierwszości używając algorytmu Millera-Rabina
 *
 * @param n Wskaźnik na liczbę wielokrotnej precyzji
 * @param n_size Liczba elementów w n
 * @param k Ilość powtórzeń testu
 */

int is_prime(unsigned char * n, int n_size, int k);
