// największa wartość, którą można zapisać na 4*16 bajtów wynosi 
// 13407807929942597099574024998205846127479365
// 82059239337772356144372176403007354697680187
// 42981669034276900318581864860508537538828119
// 46569946433649006084095
// co daje 155 cyfr. Można przechować każdą liczbę
// która ma 154 cyfry lub mniej
#define MAX_DIGITS 154
#define BIG_NUM_BYTES 4*16 // 16 razy słowo maszynowe (4 bajty)


/**
 * Wczytanie dużej liczby z pliku do buforu unsigned char
 *
 * @param filename ścieżka do pliku tekstowego z liczbą
 * @param bignum Bufor dla liczby wielokrotnej precyzji
 */

void read_number(const char* filename, char bignum[BIG_NUM_BYTES]);
