#pragma once
#include "big_num_read.h"
#include <string.h> //memcpy
#include <stdio.h>
#include <time.h> //pomiary czasu

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/*
 * Funkcja bignum_sub_s
 * -------------
 *  odejmuje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *  jeden z nich jest przedtem skalowany
 *  wynik jest liczony według wzoru
 *  r = a - b*256^s
 *
 *  a: liczba całkowita ustalonego rozmiaru 
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: bufor wyniku
 *  s: skala
 *
 *  returns: overflow (pożyczka)
 */
int bignum_sub_s(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r, int scale);

/*
 * Funkcja: bignum_add_s
 * -------------
 *  dodaje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *  jeden z nich jest przedtem skalowany
 *  wynik jest liczony według wzoru
 *  r = a + b*256^s
 *
 *  a: liczba całkowita ustalonego rozmiaru 
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: bufor wyniku
 *  s: skala
 *
 *  returns: overflow (przeniesienie)
 */
int bignum_add_s(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r, int s);

/*
 * Funkcja: bignum_sub
 * -------------
 *  odejmuje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *
 *  a: liczba całkowita ustalonego rozmiaru
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r: a - b bufor wyniku
 *
 *  returns: overflow (pożyczka)
 */
int bignum_sub(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r);

/*
 * Funkcja: bignum_add
 * -------------
 *  dodaje dwa argumenty bez znaku rozmiaru kilku słów maszynowych
 *
 *  a: liczba całkowita ustalonego rozmiaru
 *  a_size: rozmiar liczby a[] oraz r[]
 *  b: liczba całkowita ustalonego rozmiaru 
 *  b_size: rozmiar liczby b[]
 *  r:  a + b bufor wyniku
 *
 *  returns: overflow (przeniesienie)
 */
int bignum_add(const unsigned char * a, int a_size, const unsigned char * b, int b_size, unsigned char * r);

/*
 * Funkcja: bignum_div
 * -------------
 *  dzielenie dwóch argumentów bez znaku rozmiaru kilku słów i zwraca wynik
 *  używając algorytmu optymalnego dzielenia 14.20 z 
 *  Handbook of Applied Cryptography by A. Menezes, P. van Oorschot and S. Vanstone., rozdział 14
 *
 *  a, b: argumenty o ustalonym rozmiarze unsigned
 *  r: bufor na resztę z dzielenia
 *  q: bufor na iloraz
 *
 *  returns: 1 (TRUE) jeśli dzielenie jest poprawne, 0 (FALSE) jeśli nie(dzielenie przez 0)
 */
int bignum_div(const unsigned char * a, int r_size, const unsigned char * b,
        int b_size, unsigned char * r, unsigned char * q);
/*
 * Funkcja: bignum_mul
 * -------------
 *  mnożenie dwóch argumentów bez znaku rozmiaru kilku słów 
 *  używając algorytmu podobnego do 14.12 z
 *  Handbook of Applied Cryptography by A. Menezes, P. van Oorschot and S. Vanstone., rozdział 14
 *
 *  a, b: argumenty o ustalonym rozmiarze unsigned
 *  r: bufor wyniku
 */
void bignum_mul(unsigned char a[BIG_NUM_BYTES], unsigned char b[BIG_NUM_BYTES], unsigned char r[BIG_NUM_BYTES * 2]);

/*
 * Function: bignum_shl
 * --------------
 *  przesunięcie argumentu unsigned
 *
 *  n: argument o ustalonym rozmiarze unsigned
 *  r: bufor wyniku, może być taki sam jak n
 *  nr_size: rozmiar n oraz r
 *  s: ilość bitów do przesunięcia
 */ 
void bignum_shl(unsigned char * n, int n_size, unsigned char * r, int s);

/*
 * Funkcja: bignum_equ
 * ------------
 *  sprawdzenie, czy dwie liczby takiej samej długości są sobie równe
 *
 *  a, b: argumenty o ustalonej długości unsigned
 *  ab_size: rozmiary a[] i b[]
 * 
 *  returns: 1(TRUE) jeśli równe, 0(FALSE) jeśli nie
 */
int bignum_equ(unsigned char * a, unsigned char * b, int ab_size);

/* Function: bignum_zero
 * --------------
 *  sprawdzenie, czy liczba jest zerem
 *
 *  a: argument o ustalonej długości unsigned
 *  a_size: rozmiar a[]
 *  
 *  returns: 1(TRUE) jeśli równa, 0(FALSE) jeśli nie
 */
int bignum_zero(unsigned char * a, int a_size);

/**
 * Obliczenie y <- x ^ e (mod n) używając algorytmu szybkiego potęgowania modularnego
 * z Song Y. Yan, 2006. Teoria Liczb w Informatyce
 * Algorytm 2.1.1
 *
 * @param x Wskaźnik na podstawę, tablica z dużą liczbą
 * @param e Wskaźnik na potęgę, tablica z dużą liczbą
 * @param n Wskaźnik na dzielnik, tablica z dużą liczbą
 * @param y Wskaźnik na bufor wyniku, tablica z dużą liczbą
 */
void potegowanie_modulo(unsigned char * x, unsigned char * e, unsigned char * n, unsigned char * y);
